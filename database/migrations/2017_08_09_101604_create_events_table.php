<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('event_id');
            $table->string('title', 250);
            $table->string('description', 500)->nullable();
            $table->date('start_date')->nullable();

            $table->integer('circle_id')->unsigned()->nullable();
        });
        Schema::table('events', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->foreign('circle_id')
                ->references('circle_id')->on('circles')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
