<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCirclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('circles', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('circle_id');
            $table->integer('user_id')->unsigned();
            $table->string('title', 50);
            $table->string('description', 500)->nullable();
        });

        Schema::table('circles', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('circles');
    }
}
