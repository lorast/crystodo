<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPhoto extends Model
{
    protected $fillable = ['user_id', 'photoname'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
