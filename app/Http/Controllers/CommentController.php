<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function store($event_id, Request $request)
    {
        if (Auth::check()) {
            $user = Auth::user();
        }

        $event = Event::find($event_id);


        $validator = Validator::make(
            [
                'text' => $request->get('text')
            ],
            [
                'text' => 'required|max:255',
            ]
        );

        if ($validator->fails())
        {
            $messages = $validator->messages()->toArray();
            $messageText = [];

            foreach ($messages as $message) {
                $messageText[] = $message[0];
            }
            Session::flash('error_msg', implode ( ' ', $messageText));
            return Redirect::back()->withInput();
        }

        $comment = new Comment([
            'text' => $request->get('text'),
        ]);

        $comment->event()->associate($event);
        $comment->user()->associate($user);
        $comment->save();

        Session::flash('success_msg', 'Comment added successfully!');
        return Redirect::back();
    }
}
