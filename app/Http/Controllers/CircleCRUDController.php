<?php

namespace App\Http\Controllers;

use App\Circle;
use App\Event;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Session;
use Auth;
use Illuminate\Support\Facades\Validator;

class CircleCRUDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function index()
      {
            $events = Circle::all();

            return view('CircleCRUD.index', compact('circles'));
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('CircleCRUD.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            [
                'title' => $request->get('title'),
            ],
            [
                'title' => 'required',
            ]
        );

        if ($validator->fails())
        {
            Session::flash('error_msg', 'Title is required');
            return redirect(route('circle.create'))->withInput();
        }

        if (Auth::check())
        {
            $userid = Auth::id();
        }
        $circle = new Circle([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
        ]);
        $circle->user()->associate(User::find($userid))
            ->save();

        Session::flash('success_msg', 'Circle added successfully!');
        return redirect(route('index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $events = Event::all()->where('circle_id', $id);
        $circle = Circle::find($id);

        return view('CircleCRUD.show', compact('events'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $circle = Circle::find($id);
        return view('CircleCRUD.edit', compact('circle','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $circle = Circle::find($id);
        $circle->title = $request->get('title');
        $circle->description = $request->get('description');

        $circle->save();
        Session::flash('success_msg', 'Circle updated successfully!');
        return redirect(route('index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $circle = Circle::find($id);
            $circle->delete();
        } catch (QueryException $qe) {
            Session::flash('success_msg', 'Circle deleted successfully!');
            return redirect(route('index'));
        }
        Session::flash('success_msg', 'Circle deleted successfully!');
        return redirect(route('index'));
    }
}
