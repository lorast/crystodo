<?php

namespace App\Http\Controllers;

use App\Event;
use App\Favorite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class FavoriteController extends Controller
{
    public function addToFavorite($event_id)
    {
        if (Auth::check()) {
            $user = Auth::user();
        }

        $event = Event::find($event_id);

        $favorite = new Favorite([
            'user_id' => $user->id,
            'event_id' =>$event->event_id
        ]);
        $favorite->save();

        return Redirect::back();
    }

    public function removeFromFavorite($event_id)
    {
        if (Auth::check()) {
            $user = Auth::user();
        }

        $event = Event::find($event_id);

        $favorite = Favorite::where('user_id', $user->id)
            ->where('event_id', $event->event_id);

        $favorite->delete();

        return Redirect::back();
    }
}
