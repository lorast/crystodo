<?php

namespace App\Http\Controllers;

use App\User;
use App\UserPhoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function show()
    {
        if (Auth::check()) {
            $user = Auth::user();
        }

        $userPhoto = User::find($user->id)->getPhotoAttribute();
//        var_dump($userPhoto); die();
        return view('Profile.show', compact('user', 'userPhoto'));
    }

    public function uploadPhoto(Request $request)
    {
        if (Auth::check()) {
            $user = Auth::user();
        }

        $validator = Validator::make(
            [
                'photo' => Input::file('photo')
            ],
            [
                'photo' => 'required|image|mimes:jpeg,bmp,png|max:2000'
            ]
        );

        if ($validator->fails())
        {
            Session::flash('error_msg', 'The image must have a picture type and be less than 2mb');
            return Redirect::back()->withInput();
        }
        try {
            $imageName = time() . '.' . $request->photo->getClientOriginalExtension();

            $user_ = User::find($user->id);
            $user_->setPhotoAttribute($imageName);
            $user_->save();

            $request->photo->move(public_path('images/'), $imageName);
            return back()
                ->with('success', 'You have successfully upload images.');
        } catch (\Error $e) {
            Session::flash('error_msg', 'Something went wrong');
            return Redirect::back();
        } catch (\Exception $e) {
            Session::flash('error_msg', 'Something went wrong');
            return Redirect::back();
        }
    }
}
