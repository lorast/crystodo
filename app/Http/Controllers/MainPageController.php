<?php

namespace App\Http\Controllers;

use App\EventUser;
use App\Favorite;
use Illuminate\Http\Request;
use App\Event;
use App\Circle;
use Auth;
use Illuminate\Support\Facades\DB;

class MainPageController extends Controller
{
    public function index($circle_id = null)
    {
        if (Auth::check()) {
            $user = Auth::user()->id;
        }

//        $todos = DB::table('events as e')
//            ->leftjoin('circles as c','e.circle_id','=','c.circle_id')
//            ->Where('c.user_id',$user)
//            ->paginate(2);
//var_dump($todos);die();
        $circle = [];
        $circles = Circle::all()->where('user_id', $user);
        $events = [];

        if (is_null($circle_id)) {
            $events = [];
            foreach ($circles as $circle) {
                foreach (Event::all()->where('circle_id', $circle->circle_id) as $event) {
                    $events[] = $event;
                }
            }
        } elseif($circle_id == 'available') {
            $e_ids = EventUser::all()->where('user_id', $user);

            foreach ($e_ids as $e_id) {
                $events[] = $e_id->event;
            }
        } elseif($circle_id == 'favorite') {
            $e_ids = Favorite::all()->where('user_id', $user);

            foreach ($e_ids as $e_id) {
                $events[] = $e_id->event;
            }
        } elseif(is_numeric($circle_id)) {
            $circle = Circle::find($circle_id);
            $events = Event::all()->where('circle_id', $circle_id);
        }

        $events_sorted = collect($events)
            ->sortBy('start_date')
            ->reverse();
        $events = $events_sorted;

        // add array_sort($array, function($first, $second){return 1, -1}) or sort in collection
        // add pagination
        return view('mainpage', compact('events', 'circle', 'circles'));
    }
}
