<?php

namespace App\Http\Controllers;

use App\Circle;
use App\Comment;
use App\Event;
use App\EventUser;
use App\Favorite;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Mockery\Exception;
use Session;
use Auth;
use Illuminate\Support\Facades\Validator;

class EventCRUDController extends Controller
{
//    /**
//     * Display a listing of the resource.
//     *
//     * @return \Illuminate\Http\Response
//     */
//    public function index()
//    {
//        $events = Event::all();
//
//        return view('EventCRUD.index', compact('events'));
//    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (Auth::check()) {
            $user = Auth::user();
        }


        $circles = Circle::all()->where('user_id', $user->id);

        if($circles->isEmpty()) {
            Session::flash('error_msg', 'You should create a circle first');

            return redirect(route('circle.create'));
        }
        return view('EventCRUD.create', compact('circles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make(
            [
                'title' => $request->get('title'),
                'description' => $request->get('description'),
                'start_date' => $request->get('date')
            ],
            [
                'title' => 'required|max:255',
                'description' => 'max:500',
                'start_date' => 'date_format:Y-m-d',
            ]
        );

        if ($validator->fails())
        {
            $messages = $validator->messages()->toArray();
            $messageText = [];

            foreach ($messages as $message) {
                $messageText[] = $message[0];
            }
            Session::flash('error_msg', implode ( ' ', $messageText));
            return redirect(route('event.create'))->withInput();
        }

        $event = new Event([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'start_date' => $request->get('date'),
        ]);
        $event->circle()->associate(Circle::find($request->get('circleSelect')))
            ->save();

        Session::flash('success_msg', 'Event added successfully!');
        return redirect(route('index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        if (Auth::check()) {
            $user_id = Auth::user()->id;
        }

        $event_user = EventUser::where('event_event_id', $id)->get();
        $event = Event::find($id);
//var_dump($event_user); die();

        foreach ($event_user as $eu) {
            if (($eu->user->id != $user_id) &&
                ($event->circle->user_id != $user_id)){
                Session::flash('error_msg', 'Permission Denied');

                return redirect(route('index'));
            }
        }

        $comments = Comment::orderBy('id', 'desc')
            ->where('event_id', $id)
            ->simplePaginate(5);

        $eventUsers = EventUser::where('event_event_id', $id)->get();

        $favorite = Favorite::where('user_id', $user_id)
            ->where('event_id', $id)
            ->get();

        $users = [];
        foreach ($eventUsers as $item) {
            $users[] = $item->user;
        }

        return view('EventCRUD.show', compact('event', 'favorite', 'users', 'comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::find($id);
        if(Auth::check()){
            $user_id = Auth::user()->id;
        }
        if ($event->circle->user_id != $user_id) {
            Session::flash('error_msg', 'Permission Denied');

            return redirect(route('index'));
        }
        return view('EventCRUD.edit', compact('event','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = Event::find($id);
        $event->title = $request->get('title');
        $event->description = $request->get('description');
        $event->start_date = $request->get('date');
        $event->save();
        Session::flash('success_msg', 'Event updated successfully!');
        return redirect(route('index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);
        $event->delete();
        Session::flash('success_msg', 'Event deleted successfully!');
        return redirect(route('index'));
    }

    /**
     * Search events by request
     */
    public function search(Request $request)
    {
        if (Auth::check()) {
            $user = Auth::user();
        }

        $event_title = $request->get('event-title');
        $event_date = $request->get('date');

        try {
            if (!empty($event_title)) {
                $circles = Circle::all()->where('user_id', $user->id);

                $events = [];
                foreach ($circles as $circle) {
                    foreach ((Event::where('circle_id', $circle->circle_id)
                        ->where('title', 'LIKE', '%' . $event_title . '%')->get()) as $event) {
//                    var_dump($event);die();
                        $events[] = $event;
                    }
                }
            } elseif (!empty($event_date)) {
                $circles = Circle::all()->where('user_id', $user->id);

                $events = [];
                foreach ($circles as $circle) {
                    foreach ((Event::where('circle_id', $circle->circle_id)
                        ->where('start_date', $event_date)->get()) as $event) {
//                    var_dump($event);die();
                        $events[] = $event;
                    }
                }
            }
        } catch(Error $e) {
            Session::flash('error_msg', "Something went wrong");

            return Redirect::back();
        }
        return view('EventCRUD.search', compact('events'));
    }
}
