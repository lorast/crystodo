<?php

namespace App\Http\Controllers;

use App\Circle;
use App\Event;
use App\Mail\AddUser;
use App\User;
use App\EventUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Mockery\Exception;
use Session;
use Auth;
use Illuminate\Support\Facades\Validator;

class InviteController extends Controller
{
    /**
     * @param null $circle_id
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function index($circle_id = null)
    {
        $circle = Circle::find($circle_id);

        $events = Event::all()->where('circle_id', $circle_id);

        return view('Invite.index', compact('events', 'circle'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function invite(Request $request)
    {
        $validator = Validator::make(
            [
                'user_name' => $request->get('user_name'),
//                'events' => $request->get('events_ti')
            ],
            [
                'user_name' => 'required|max:255',
//                'events' => 'required|array'
            ]
        );

        if ($validator->fails()) {
            $messages = $validator->messages()->toArray();
            $messageText = [];

            foreach ($messages as $message) {
                $messageText[] = $message[0];
            }
            Session::flash('error_msg', implode ( ' ', $messageText));
            return Redirect::back()->withInput();
        }

        $user_name = $request->get('user_name');
        $user = User::where('name', $user_name)->first();

        if (!$user) {
            Session::flash('error_msg', 'Have no users named as "' . $user_name . '"');
            return Redirect::back()->withInput();
        }

        $event_id = $request->get('event_id');

        $checkExists = EventUser::where('user_id', $user->id)
            ->where('event_event_id', '=', $event_id)
            ->first();
//var_dump($checkExists); die();
        if(count($checkExists) > 0) {
            Session::flash('error_msg', 'This user already added');

            return Redirect::back()->withInput();
        } else {
            $user_ = User::find($user->id);
            $user_->events()->attach($event_id);
            $user_->save();
            $event_user = DB::table('event_user')
                ->where('user_id', $user_)
                ->where('event_event_id', $event_id);
            $event_user->update(['roles' => 'viewer']);
        }

        $email = $user->email;
        try {
            \Mail::to($email)->send(new AddUser($user));
        }catch (Error $e) {
            Session::flash('error_msg', $e);
            return Redirect::back();
        }

        Session::flash('success_msg', 'User was added.');
        return redirect(route('index'));
    }

    /**
     * @param $event_id
     * @param $user_id
     * @return mixed
     */
    public function delete($event_id, $user_id)
    {
        $event_user = EventUser::where('event_event_id', $event_id)
            ->where('user_id', $user_id);

        try {
            $event_user->delete();

            return Redirect::back();
        } catch (Exception $e) {
            echo $e;
        }
    }

    public function search(Request $request)
    {
        $userpart = $request->get('user_name');

        $users = User::where('name', 'LIKE', '%' . $userpart . '%')
            ->get();

        $data = '';

        foreach ($users as $user) {
            $data = $user->name;
        }

        if(count($data))
            return $data;
        else
            return 'No Result Found';

        var_dump($user);die();
    }
}
