<?php

namespace App\Http\Controllers;

use App\Event;
use App\EventUser;
use App\Mail\EventStarts;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class EmailController extends Controller
{
    public function sendEmailsWhenStarts()
    {
        $events = Event::where('start_date', '<=', Carbon::now())->get();

//        $sharedEvents = DB::table('');

        foreach ($events as $event){
            $email = null;
            $email = $event->circle->user->email;
            var_dump('Sent message on '. $email);
                try {
                    \Mail::to($email)->send(new EventStarts($event->title));
                } catch (Error $e) {
                    Session::flash('error_msg', $e);
                    return Redirect::back();
                }
            $eventUsers = EventUser::where('event_event_id', $event->event_id)->get();
                foreach ($eventUsers as $eventUser) {
                    $email = $eventUser->user->email;
                    try {
                        \Mail::to($email)->send(new EventStarts($event->title));
                    } catch (Error $e) {
                        Session::flash('error_msg', $e);
                        return Redirect::back();
                    }
                    var_dump('Message  sent to added user ' . $email);
                }
        }
    }
}
