<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
//    public $timestamps = false;
    protected $fillable = ['text'];
    protected $primaryKey = 'comment_id';

    public function event()
    {
        return $this->belongsTo('App\Event', 'event_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
