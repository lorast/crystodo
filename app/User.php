<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getNameAttribute()
    {
        return $this->attributes['name'];
    }

    public function getEmailAttribute()
    {
        return $this->attributes['email'];
    }

    public function setPhotoAttribute($value)
    {
        $this->attributes['photo'] = $value;
    }

    public function getPhotoAttribute()
    {
        return $this->attributes['photo'];
    }

    public function circles()
    {
        return $this->hasMany('App\Circle');
    }

    public function events()
    {
        return $this->belongsToMany('App\Event');
    }
}
