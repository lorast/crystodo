<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventStarts extends Mailable
{
    use Queueable, SerializesModels;

    private $event_name;

    /**
     * Create a new message instance.
     * @param $event_name
     */
    public function __construct($event_name)
    {
        $this->event_name = $event_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Email.eventstarts', [
            'event_name' => $this->event_name
        ]);
    }
}
