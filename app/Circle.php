<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Circle extends Model
{
    public $timestamps = false;
    protected $fillable = ['title', 'description'];
    protected $primaryKey = 'circle_id';

    public function events()
    {
        return $this->hasMany('App\Event');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function boot()
    {
        parent::boot();

        static::deleted(function ($model) {
            // Probably lazy load these relationships to avoid lots of queries?
            $model->load([ 'events' ]);

            $model->events()->delete();
        });
    }
}
