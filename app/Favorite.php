<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    public $timestamps = false;

    protected $table = 'favorites';

    protected $fillable = ['user_id', 'event_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function event()
    {
        return $this->belongsTo('App\Event', 'event_id');
    }
}
