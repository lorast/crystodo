<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public $timestamps = false;
    protected $fillable = ['title', 'description', 'start_date'];
    protected $primaryKey = 'event_id';

    public function circle()
    {
        return $this->belongsTo('App\Circle', 'circle_id');
    }

    public function user()
    {
        return $this->belongsToMany('App\User');
    }
}
