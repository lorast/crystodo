var searchRequest = null;

$(function () {
    var minlength = 3;

    $("#userinput").keyup(function () {
        var that = this,
            value = $(this).val();

        if (value.length >= minlength ) {
            if (searchRequest != null)
                searchRequest.abort();
            searchRequest = $.ajax({
                type: "GET",
                url: "{{ route('invite.search') }}",
                data: {
                    'user_name' : value
                },
                dataType: "text",
                success: function(data) {
                    response(data);

                }
            });
        }
    });
});