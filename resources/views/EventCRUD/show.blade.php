@extends('layouts.app')
@section('content')
    <div class="container event-show">
        <h4>{{ $event->title }}</h4>
        @if ($event->description)
            <p class="col col-sm-6 col-sm-offset-3 alert-info"
               style="padding: 10px; border-radius: 5px;">{{ $event->description }}</p>
        @else
            <p class="col col-sm-6 col-sm-offset-3 alert-warning"
               style="padding: 10px; border-radius: 5px;">this event has no description</p>
        @endif
        <div class="col col-sm-3">
            @if(count($favorite) > 0)
                <a href="{{ route('favorite.remove', $event->event_id) }}">
                    <span title="Remove from favorites" class="favorite-star glyphicon glyphicon-star"></span>
                </a>
            @else
                <a href="{{ route('favorite.add', $event->event_id) }}">
                    <span title="Add to favorites" class="favorite-star glyphicon glyphicon-star-empty"></span>
                </a>
            @endif
        </div>
        <div class="col col-sm-12">
            <p class="edit-show-date">
                <span>Start at:</span>
                <br>
                {{ $event->start_date }}
            </p>
        </div>
        <div class="col col-sm-12">
            <hr>
            <div class="users-block">
                <h4 class="pull-top">Users:</h4>
                <div class="user_inv">
                    @if ($event->circle->user->getPhotoAttribute())
                    <img src="{{ asset('images/'.$event->circle->user->getPhotoAttribute()) }}" >
                    @else
                        {{ $event->circle->user->name }}
                    @endif
                </div>
                @foreach($users as $user)
                <div class="user_inv">
                    <img src="{{ asset('images/'.$user->getPhotoAttribute()) }}" >
                    @if(Auth::user()->id == $event->circle->user->id)
                        <a onclick="return confirm('Are you sure to delete this user?')" href="{{ url('invite/delete/' . $event->event_id . '/' . $user->id) }}">
                            <span class="remove-user glyphicon glyphicon-remove"></span>
                        </a>
                    @endif
                </div>
                @endforeach
                @if(Auth::user()->id == $event->circle->user->id)
                    <a href="{{ route('invite', $event->circle->circle_id) }}"><h5>Add users</h5></a>
                @endif
            </div>
        </div>
    </div>
    <div class="container">
        <hr>
        <h4>Comments:</h4>
        <hr>
        @include('parts.createCommentForm')
        @if($comments)
            @include('parts.showComments')
        @else
            <p>No one</p>
        @endif
        {{ $comments->render() }}
    </div>
@endsection