@extends('layouts.app')
@section('content')
    @if (Auth::check())
        <div class="sidepanel col col-xs-2 col-xs-offset-1">
            @include('parts.sidepanel')
        </div>
    @endif

    @if ($f = session('success_msg'))
        <div id="flashMsg" class="alert alert-success" role="alert">
            {{ $f }}
        </div>
    @endif

    <a href="{{ route('event.create') }}"> create new</a>
    @foreach($events as $e)
    <div class="col col-xs-6 col-xs-offset-1">
        <div class="panel">
            <div class="row">
                <div class="col col-xs-offset-1 col-xs-6 panel-title">
                    <h4>
                        {{ $e->title }}
                    </h4>
                </div>
                <div class="col col-xs-3">
                    {{ $e->start_date }}
                </div>
                <div class="col col-xs-2">
                    @if($e->circle_id == 0)
                        Stack
                    @endif
                </div>
            </div>
            <div class="panel-body">
                {{ $e->description }}
            </div>
        </div>
    </div>
    <div class="col col-xs-1">
        <a href="{{ route('event.edit', $e->event_id) }}" class="btn btn-warning">Edit</a>
        <a href="{{ route('event.destroy', $e->event_id) }}" class="btn btn-danger"
           onclick="return confirm('Are you sure to delete?')">Delete</a>
    </div>
    @endforeach
@endsection