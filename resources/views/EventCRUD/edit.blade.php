@extends('layouts.app')
@section('content')
    <div class="container">
        <form method="get" action="{{action('EventCRUDController@update', $id)}}">
            <div class="form-group row">
                {{csrf_field()}}
                <input name="_method" type="hidden" value="PATCH">
                <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Title</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="title" value="{{ $event->title }}" name="title">
                </div>
            </div>
            <div class="form-group row">
                <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Description</label>
                <div class="col-sm-8">
                    <textarea name="description" class="form-control" rows="8" cols="80">{{ $event->description }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Start date</label>
                <div class="col-sm-8">
                    <input placeholder="YYYY-MM-DD" class="datepicker form-control form-control-lg" id="lgFormGroupInput"
                           name="date" value="{{ $event->start_date }}">
                </div>
            </div>
            <div class="form-group row">
                <input type="submit" class="btn btn-primary" value="Update Event">
            </div>
        </form>
    </div>
@endsection