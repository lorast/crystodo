@extends('layouts.app')
@section('content')
    <div class="container">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Description</th>
                <th>Date</th>
                <th colspan="2">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($events as $e)
                <tr>
                    <td>{{$e->id}}</td>
                    <td>{{$e->title}}</td>
                    <td>{{$e->description}}</td>
                    <td>{{$e->date}}</td>
                    <td><a href="{{ route('event.edit, $e->event_id')}}" class="btn btn-warning">Edit</a></td>
                    <td>
                        <form action="{{ route('event.destroy', $e->event_id)}}" method="post">
                            {{csrf_field()}}
                            <input name="_method" type="hidden" value="DELETE">
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection