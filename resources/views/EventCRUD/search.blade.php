@extends('layouts.app')
@section('content')
    <div class="container">
        <form action="{{ route('event.search') }}" method="get" >
            <h4>Search by date:</h4>
            <div class="col col-sm-offset-4 col-sm-4">
                <input placeholder="YYYY-MM-DD" class="datepicker form-control" id="lgFormGroupInput"
                       name="date" value="{{ old('date') }}">
                <br>
            </div>
            <div class="col col-sm-12">
                <input type="submit" value="Search" class="btn btn-primary">
                <br><br>
            </div>
        </form>
        @if (count($events) > 0)
            <hr>
            <h3>Search results: </h3>
            <hr>
            @foreach($events as $event)
                <div class="col col-sm-offset-3 col-sm-6">
                    <h4>
                        <a href="{{ route('event.show', $event->event_id) }}">
                        {{ $event->title }}
                        </a>
                    </h4>
                </div>
            @endforeach
        @else
            <div class="col col-sm-12">
                <hr>
                <p>No results.</p>
                <hr>
            </div>

        @endif
    </div>
@endsection