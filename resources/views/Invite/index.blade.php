@extends('layouts.app')
@section('content')
    @if ($f = session('error_msg'))
        <div id="flashMsg" class="alert alert-danger" role="alert">
            {{ $f }}
        </div>
    @endif
    @if($errors->any())
        <h4>{{$errors->first()}}</h4>
    @endif
    <div class="container">
        <form action="{{ route('invite.invite') }}" method="post">
            {{csrf_field()}}
            <h4>Active circle: {{ $circle->title }}</h4>
            <h4>Select event to share:</h4>
            <div class="form-group row">
                <div class="col col-sm-offset-4 col-sm-4">
                    <select name="event_id" class="form-control">
                        @foreach($events as $event)
                            <option value="{{ $event->event_id }}">
                                {{ $event->title }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col col-sm-offset-4 col-sm-4">
                    <h4>Enter user name to invite:</h4>
                    <input type="text" id="user_name" name="user_name" class="form-control" placeholder="User name">
                </div>
            </div>
            <div class="form-group row">
                <input value="Add user" type="submit" class="btn btn-primary">
            </div>
            <div class="alert alert-info col col-sm-offset-3 col-sm-6">
                Suggestions: <span class="user-variants"></span>
            </div>
        </form>
    </div>
@endsection
@section('js_script')
    <script>
        $(document).ready(function() {
            var searchRequest = null;

            $(function () {
                var minlength = 3;

                $("#user_name").keyup(function () {
                    var that = this,
                        value = $(this).val();

                    if (value.length >= minlength ) {
                        if (searchRequest != null)
                            searchRequest.abort();
                        searchRequest = $.ajax({
                            type: "GET",
                            url: "{{ route('invite.search') }}",
                            data: {
                                'user_name' : value
                            },
                            dataType: "text",
                            success: function(data) {
                                $('to_fill').show();
                                $('.user-variants').empty().append(data);

                            }
                        });
                    }
                });
            });
        });
    </script>
@endsection