@extends('layouts.app')

@section('content')
    <div class="col col-xs-offset-4 col-xs-6">
        @if (Auth::check())
            <span id="welcomeText">
            Hello, {{ Auth::user()->name }}! <br>
            Now, you can start your work.
        </span>
            <br>
            <a href="{{ route('index') }}" class="btn btn-success">Let's work!</a>
        @else
            <div class="row">
                <span> now, you can </span>
                <a href="{{ route('login') }}" class="btn btn-default">LOGIN</a>
                <span>or</span>
                <a href="{{ route('register') }}" class="btn btn-primary">REGISTER</a>
                <span>before working starts</span>
                @endif
            </div>
@endsection
