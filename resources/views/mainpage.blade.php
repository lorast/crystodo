@extends('layouts.app')
@section('content')
    @if (Auth::check())
        <div class="sidepanel col col-xs-2 col-xs-offset-1">
            @include('parts.sidepanel')
        </div>
    @endif

    @if ($f = session('success_msg'))
        <div id="flashMsg" class="alert alert-success" role="alert">
            {{ $f }}
        </div>
    @endif
    @if ($f = session('error_msg'))
        <div id="flashMsg" class="alert alert-danger" role="alert">
            {{ $f }}
        </div>
    @endif
    @if($circle and Request::is('index/*'))
        <h4>
            Active circle: {{ $circle->title }}
            <a href="{{ route('circle.edit', $circle->circle_id) }}">
                <span class="glyphicon glyphicon-cog" style="font-size: 1.5em; vertical-align: text-bottom;"></span>
            </a>
            <hr>
        </h4>
    @endif
    <div class="row">
        @if(count($events) > 0)
            @foreach($events as $e)
                <div class="col-sm-2 col-md-2">
                    <div class="thumbnail @if ($e->start_date > Carbon\Carbon::now()) active @else expired @endif">
                        <span class="glyphicon glyphicon-bell title"></span>
                        <div class="caption">
                            <h4><a href="{{ route('event.show', $e->event_id) }}">
                                    {{ $e->title }}
                                </a>
                            </h4>
                            <div class="circle_label">
                                {{ $e->circle->title }}
                            </div>
                            <p class="date">{{ $e->start_date }}</p>
                            @if(Auth::user()->id == $e->circle->user->id)
                                <div class="">
                                    <a href="{{ route('event.edit', $e->event_id) }}" class="btn btn-warning">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                    <a href="{{ route('event.destroy', $e->event_id) }}" class="btn btn-danger"
                                       onclick="return confirm('Are you sure to delete?')">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <hr>
            <p>no one</p>
            <hr>
        @endif
    </div>
    <a href="{{ route('event.create') }}" id="addEvent" class="btn btn-primary">Create new</a>
@endsection