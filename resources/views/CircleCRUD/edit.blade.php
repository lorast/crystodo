@extends('layouts.app')
@section('content')
    <div class="row container">
        <form method="get" action="{{action('CircleCRUDController@update', $id)}}">
            <div class="form-group row">
                {{csrf_field()}}
                <input name="_method" type="hidden" value="PATCH">
                <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Title</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="title" value="{{ $circle->title }}" name="title">
                </div>
            </div>
            <div class="form-group row">
                <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Description</label>
                <div class="col-sm-6">
                    <textarea class="form-control" name="description" rows="8" cols="67">{{ $circle->description }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2"></div>
                <input type="submit" class="btn btn-primary" value="Update">
            </div>
            <div class="form-group row">
                <div class="col-md-2"></div>
                <a href="{{ route('circle.delete', $circle->circle_id) }}"  onclick="return confirm('Are you sure to delete?')" class="btn btn-danger" value="Delete">Delete</a>
            </div>
        </form>
    </div>
    @include('parts.inviteblock')

@endsection