@extends('layouts.app')
@section('content')
@if ($f = session('error_msg'))
    <div id="flashMsg" class="alert alert-danger" role="alert">
        {{ $f }}
    </div>
@endif
    <div class="container">
        <form method="post" action="{{ route('circle.store') }}">
            <div class="form-group row">
                {{csrf_field()}}
                <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Title</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="title"
                           name="title" value="{{ old('title') }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Description</label>
                <div class="col-sm-8">
                    <textarea name="description" class="form-control" rows="8" cols="80">{{ old('description') }}
                    </textarea>
                </div>
            </div>

            <div class="form-group row">
                <input value="Create Circle" type="submit" class="btn btn-primary">
            </div>
        </form>
    </div>
@endsection