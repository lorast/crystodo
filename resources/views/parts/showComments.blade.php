@foreach($comments as $comment)
    <div class="row comment">
        <div class="col-xs-offset-2 col-xs-2">
            <div class="user-photo">
                <img src="{{ asset('images/'.$comment->user->getPhotoAttribute()) }}" >
            </div>
            <p class="user-name">{{$comment->user->name}}</p>
        </div>
        <div class="col-xs-5 textComment">
            <div class="front"></div>
            {{ $comment->text }}
            <p class="date rull-right">{{ $comment->created_at }}</p>
        </div>
    </div>
@endforeach