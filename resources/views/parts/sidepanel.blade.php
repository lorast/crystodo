<ul class="list-group">
    <a href="{{ route('index') }}">
        <li class="{{ Request::is('index') ? 'activeBlock ' : '' }}circleBlock">
            ALL MY EVENTS
        </li>
    </a>
    <a href="{{ route('index', 'available') }}">
        <li class="{{ Request::is('index/available') ? 'activeBlock ' : '' }}circleBlock">
            AVAILABLE
        </li>
    </a>
    <a href="{{ route('index', 'favorite') }}">
        <li class="{{ Request::is('index/favorite') ? 'activeBlock ' : '' }}circleBlock">
            FAVORITES
        </li>
    </a>
    <hr>
    <h4>Circles</h4>
    <hr>
    @foreach($circles->slice(0, 10) as $c)
            <a href="{{ route('index', $c->circle_id) }}">
                <li class="{{ Request::is('index/'.$c->circle_id) ? 'activeBlock ' : '' }} circleBlock">
                    {{ $c->title }}
                </li>
            </a>
    @endforeach
    <hr>
    <li><a href="{{ route('circle.create') }}" class="spButtons">Add new circle </a></li>
</ul>


