<div class="panel panel-default">
    <div class="panel-body">
        <form action="{{ route('comment.store', $event->event_id) }}" method="post" class="form-inline">
            {{csrf_field()}}
            <div class="form-group">
                <label for="lgFormGroupInput">Comment:</label>
                <input type="text" class="form-control" placeholder="say some words..."
                           name="text" value="{{ old('text') }}">
            </div>
            <input value="Add comment" type="submit" class="btn btn-primary">
        </form>
    </div>
</div>