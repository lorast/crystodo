<div class="row">
    <div class="alert alert-success" role="alert">
        <p>You can add user to some events from your circle.</p>
        <h3><a href="{{ route('invite', $circle->circle_id) }}">Add users</a></h3>
    </div>
</div>