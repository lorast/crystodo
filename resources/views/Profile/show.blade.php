@extends('layouts.app')
@section('content')
    @if ($f = session('error_msg'))
        <div id="flashMsg" class="alert alert-danger" role="alert">
            {{ $f }}
        </div>
    @endif
    <div class="thumbnail col-sm-offset-4 col-sm-4">
        @if(!$userPhoto)
            <img class="userProfilePhoto" src="{{ asset('images/defaults/default-avatar.png') }}" alt="">
        @else
            <img class="userProfilePhoto" src="{{ asset('images/'.$userPhoto)}}" alt="">
        @endif
            <form action="{{ route('profile.uploadPhoto') }}" method="post" enctype="multipart/form-data">
               {{csrf_field()}}
                <div class="form-group">
                    <div class="form-control">
                        <input type="file" name="photo" class="btn">
                    </div>
                    <br>
                    <input type="submit" class="btn btn-success" value="Upload photo">
                </div>
            </form>
            <hr>

        <div>Username: <b>{{ $user->name }}</b></div>
        <div>E-mail: <b>{{ $user->email }}</b></div>
    </div>
@endsection