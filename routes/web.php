<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Route for view main job page
Route::get('/index/{circle_id?}', 'MainPageController@index')
    // Only authenticated users can view this
    ->middleware('auth')
    ->name('index');

Route::get('/invite/{circle_id?}', 'InviteController@index')
    ->middleware('auth')
    ->name('invite');
Route::post('/invite/invite', 'InviteController@invite')
    ->middleware('auth')
    ->name('invite.invite');
Route::get('/invite/delete/{event_id}/{user_id}', 'InviteController@delete')
    ->middleware('auth')
    ->name('invite.delete');
Route::get('/invite_search', 'InviteController@search')
    ->middleware('auth')
    ->name('invite.search');

// Event CRUD routes
Route::get('/event/show/{id}', 'EventCRUDController@show')
    ->middleware('auth')
    ->name('event.show');
Route::get('/event/create', 'EventCRUDController@create')
    ->middleware('auth')
    ->name('event.create');
Route::post('/event/store', 'EventCRUDController@store')
    ->middleware('auth')
    ->name('event.store');
Route::get('/event/edit/{id}', 'EventCRUDController@edit')
    ->middleware('auth')
    ->name('event.edit');
Route::get('/event/update/{id}', 'EventCRUDController@update')
    ->middleware('auth')
    ->name('event.update');
Route::get('/event/destroy/{id}', 'EventCRUDController@destroy')
    ->middleware('auth')
    ->name('event.destroy');
Route::get('/event/search', 'EventCRUDController@search')
    ->middleware('auth')
    ->name('event.search');

// Circle CRUD routes
Route::get('/circle/show/{id}', 'CircleCRUDController@show')
    ->middleware('auth')
    ->name('circle.show');
Route::get('/circle/create', 'CircleCRUDController@create')
    ->middleware('auth')
    ->name('circle.create');
Route::post('/circle/store', 'CircleCRUDController@store')
    ->middleware('auth')
    ->name('circle.store');
Route::get('/circle/edit/{id}', 'CircleCRUDController@edit')
    ->middleware('auth')
    ->name('circle.edit');
Route::get('/circle/update/{id}', 'CircleCRUDController@update')
    ->middleware('auth')
    ->name('circle.update');
Route::get('/circle/delete/{circle_id}', 'CircleCRUDController@destroy')
    ->middleware('auth')
    ->name('circle.delete');

// CommentController Routes
Route::post('/comment/store/{event_id}', 'CommentController@store')
    ->middleware('auth')
    ->name('comment.store');

// User ProfileController Routes
Route::get('/profile/show', 'ProfileController@show')
    ->middleware('auth')
    ->name('profile.show');
Route::post('/profile/upload-photo', 'ProfileController@uploadPhoto')
    ->middleware('auth')
    ->name('profile.uploadPhoto');

// FavoriteController Routes
Route::get('/favorite/{event_id}', 'FavoriteController@addToFavorite')
    ->middleware('auth')
    ->name('favorite.add');
Route::get('/favorite/remove/{event_id}', 'FavoriteController@removeFromFavorite')
    ->middleware('auth')
    ->name('favorite.remove');

// CRON Routes
Route::get('/send-notification', 'EmailController@sendEmailsWhenStarts')
    ->middleware('auth')
    ->name('send.notification');